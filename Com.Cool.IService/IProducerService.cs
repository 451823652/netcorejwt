﻿using Com.Common.IService;
using System;

namespace Com.Cool.IService
{
    public interface IProducterService:IDependency
    {
        void Start();
        object GetList();
    }
}
