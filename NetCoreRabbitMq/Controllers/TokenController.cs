﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using IdentityModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using NetCoreRabbitMq.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NetCoreRabbitMq.Controllers
{
    [Route("api/[controller]")]
    public class TokenController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult GenToken(UserModel model)
        {
            //前端传过来的，如果账号密码在数据库中，取出再判断
            var user =  new UserModel() { ID = 1, Email = "example@live.com", Name = "tester", PassWord = "123456" };
            //if user null ,return
            //var user = new UserModel { ID = o, Email =null, Name = null, PassWord = null };
            if (user == null) return Unauthorized();              
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("this is a security key");//加密key
            var authTime = DateTime.UtcNow;//token签发时间
           // var expiresAt = authTime.AddDays(7);
            var expiresAt = authTime.AddMinutes(1);//设置token的到期时间为一分钟
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(JwtClaimTypes.Audience,"api"),
                    new Claim(JwtClaimTypes.Issuer,"http://localhost:44319"),
                    new Claim(JwtClaimTypes.Id, user.ID.ToString()),
                    new Claim(JwtClaimTypes.Name, user.Name)
                }),
                Expires = expiresAt,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)//使用私钥进行签名加密
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);  //生成Token
            var tokenString = tokenHandler.WriteToken(token);
            return Ok(new
            {
                access_token = tokenString,
                token_type = "Bearer",
                profile = new
                {
                    sid = user.ID,
                    name = user.Name,
                    auth_time = new DateTimeOffset(authTime).ToUnixTimeSeconds(),
                    expires_at = new DateTimeOffset(expiresAt).ToUnixTimeSeconds()
                }
            });
        }
    }
}
