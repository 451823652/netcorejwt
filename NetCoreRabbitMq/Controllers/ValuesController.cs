﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Com.Cool.IService;
using Com.Youth.IService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
namespace NetCoreRabbitMq.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ValuesController : ControllerBase
    {
        #region acutofac 构造函数注入
        public IProducterService producter;
        public ITestService testService;
        public ValuesController(IProducterService producter, ITestService testService)
        {
            this.producter = producter;
            this.testService = testService;
        }
        #endregion
        // GET api/values
        [HttpGet]
        public string Get()
        {
            var data = new
            {
                a = producter.GetList(),
                b = testService.user(),
                time=DateTime.Now
            };
            return JsonConvert.SerializeObject(data);
        }
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
