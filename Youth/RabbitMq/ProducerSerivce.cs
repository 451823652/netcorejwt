﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Cool.IService;
using RabbitMQ;
using RabbitMQ.Client;

namespace NetCoreRabbitMq.RabbitMq
{
    public class ProducerService : IProducterService
    {
        ConnectionFactory factory;
        public ProducerService()
        {

            //factory = new ConnectionFactory
            //{
            //    UserName = "guest",
            //    Password = "guest",
            //    HostName = "localhost"
            //};
        }

        public void Start()
        {
            //创建连接
            var connection = factory.CreateConnection();
            //创建通道
            var channel = connection.CreateModel();
            //声明一个队列
            channel.QueueDeclare("hello", false, false, false, null);
            string input = "Hello,Rabbit,wo shi";



            var sendBytes = Encoding.UTF8.GetBytes(input);
            //发布消息
            channel.BasicPublish("", "hello", null, sendBytes);
            channel.Close();
            connection.Close();
        }


        public object GetList()
        {
            var user = new
            {
                name = "xiong",
                age = "18"
            };
            return user;
        }
    }
}
